from django.shortcuts import render
from django.http import HttpResponse
from insta.models import Hashtags
import re

def hashtags(request, hashtag = 'ferrari'):
    res, next_ = Hashtags.api.tag_recent_media(100, "" ,hashtag)
    
    return render(request, 'insta/hashtags.html', {'res': res, 'hashtag':hashtag})
