from django.db import models
from instagram.client import InstagramAPI
import sys

if len(sys.argv) > 1 and sys.argv[1] == 'local':
    try:
        from test_settings import *
        InstagramAPI.host = test_host
        InstagramAPI.base_path = test_base_path
        InstagramAPI.access_token_field = "access_token"
        InstagramAPI.authorize_url = test_authorize_url
        InstagramAPI.access_token_url = test_access_token_url
        InstagramAPI.protocol = test_protocol
    except Exception:
        pass

# Fix Python 2.x.
try:
    import __builtin__
    input = getattr(__builtin__, 'raw_input')
except (ImportError, AttributeError):
    pass


class Hashtags(models.Model):
    client_id = "91b9fbb4ccef4862a0b308235054f0eb"
    client_secret = "fb58d8a1d9504a5a81dd2eb890f8cf3d"
    redirect_uri = "http://localhost:8000/instagram/"
    raw_scope = ""
    scope = raw_scope.split(' ')
    # For basic, API seems to need to be set explicitly
    if not scope or scope == [""]:
        scope = ["basic"]
    api = InstagramAPI(client_id=client_id, client_secret=client_secret, redirect_uri=redirect_uri)
    redirect_uri = api.get_authorize_login_url(scope = scope)

    #print ("Visit this page and authorize access in your browser: "+ redirect_uri)

    #code = (str(input("Paste in code in query string after redirect: ").strip()))
    def get_access_token(url):
        print ("getting code: " )
        access_token = api.exchange_code_for_access_token(code)
        print ("access token: " )
        print (access_token)
