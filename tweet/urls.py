from django.conf.urls import patterns, url
from tweet import views

urlpatterns = patterns('',

    #This is the default route for /twitter/
    #For now, it's not in use, so it's disabled
    
    #url(r'^$', views.index, name='index'),

    #When a user accesses /twitter/hashtags/tag_name, the tag_name is stored
    #as a string in the variable "name"

    url(r'^hashtags/(?P<hashtag>\w*)', views.hashtags, name='hashtag'),
)
