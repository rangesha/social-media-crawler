from django.shortcuts import render
from django.http import HttpResponse
from tweet.models import Hashtags

def hashtags(request, hashtag = 'ferrari'):
     #hashtag = request.GET.get('hashtag', 'ferrari')
     res = Hashtags.api.GetSearch('%23'+hashtag)
     return render(request, 'tweet/hashtags.html', {'res': res, 'hashtag':hashtag})
