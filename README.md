## Crawler

This is a Django application that gathers information from Facebook and Twitter and displays them in a human readable format. The framework used for the interface of this project is Bootstrap.

### Documentation

There are two documents associated with this project:

1) Installation documentation

2) Project documentation

#### Installation documentation

This documentation provides an overview of how this project was setup from ground up.

#### Project documentation

This documentation explains the logic of the code used in this project.

### Assumptions

This project's documentation makes the following assumptions:

1. You are a programmer and you understand the languages used in this project - Python, HTML, Javascript and CSS.
2. You are familiar with frameworks and in particular, Django.
3. You are familiar with the MVC paradigm.
